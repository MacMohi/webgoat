stages:
  - build
  - test
  - security
  - deploy

variables:
  SCAN_BRANCHES: "/^(main|stage|release)$/"

cache:
  paths:
    - .m2/repository/
    - target/

image: maven:latest

build:
  stage: build
  script: mvn -B compile

test:
  stage: test
  script: mvn -B test

deploy:
  stage: deploy
  only:
    variables:
      - $CI_COMMIT_BRANCH =~ $SCAN_BRANCHES
  script: mvn -B install

.coverity-bac:
  stage: security
  rules:
    - if: ($CI_COMMIT_BRANCH =~ $SCAN_BRANCHES && $CI_PIPELINE_SOURCE != "merge_request_event")
  variables:
    COVERITY_PROJECT: "$CI_PROJECT_NAME"
    COVERITY_STREAM: "$CI_PROJECT_NAME-$CI_COMMIT_BRANCH"
    COV_TOOLKIT: "cov-analysis-linux64-2023.3.0"
  script:
    - export COVERITY_URL=https://coverity-aws.chuckaude.com
    - curl -fLsS --user $COV_USER:$COVERITY_PASSPHRASE $COVERITY_URL/downloadFile.htm?fn=$COV_TOOLKIT.tar.gz | tar -C /tmp -xzf -
    - curl -fLsS --user $COV_USER:$COVERITY_PASSPHRASE -o /tmp/$COV_TOOLKIT/bin/license.dat $COVERITY_URL/downloadFile.htm?fn=license.dat
    - export PATH=$PATH:/tmp/$COV_TOOLKIT/bin
    - cov-configure --java
    - cov-configure --javascript
    - cov-build --dir idir --fs-capture-search $CI_PROJECT_DIR mvn -B -DskipTests clean package
    - cov-analyze --dir idir --ticker-mode none --strip-path $CI_PROJECT_DIR --webapp-security
    - cov-commit-defects --dir idir --ticker-mode none --url $COVERITY_URL --stream $COVERITY_STREAM --scm git
        --description $CI_PIPELINE_URL --target $CI_RUNNER_EXECUTABLE_ARCH --version $CI_COMMIT_SHORT_SHA

.coverity-cli:
  stage: security
  rules:
    - if: ($CI_COMMIT_BRANCH =~ $SCAN_BRANCHES && $CI_PIPELINE_SOURCE != "merge_request_event")
  variables:
    COVERITY_PROJECT: "$CI_PROJECT_NAME"
    COVERITY_STREAM: "$CI_PROJECT_NAME-$CI_COMMIT_BRANCH"
    COV_TOOLKIT: "cov-analysis-linux64-2023.3.0"
  script:
    - export COVERITY_URL=https://coverity-aws.chuckaude.com
    - curl -fLsS --user $COV_USER:$COVERITY_PASSPHRASE $COVERITY_URL/downloadFile.htm?fn=$COV_TOOLKIT.tar.gz | tar -C /tmp -xzf -
    - curl -fLsS --user $COV_USER:$COVERITY_PASSPHRASE -o /tmp/$COV_TOOLKIT/bin/license.dat $COVERITY_URL/downloadFile.htm?fn=license.dat
    - /tmp/$COV_TOOLKIT/bin/coverity scan -o commit.connect.url=$COVERITY_URL -o commit.connect.stream=$COVERITY_STREAM

.coverity-thin-client:
  stage: security
  rules:
    - if: ($CI_COMMIT_BRANCH =~ $SCAN_BRANCHES && $CI_PIPELINE_SOURCE != "merge_request_event")
  variables:
    COVERITY_PROJECT: "$CI_PROJECT_NAME"
    COVERITY_STREAM: "$CI_PROJECT_NAME-$CI_COMMIT_BRANCH"
    CTC_TOOLKIT: "cov_thin_client-linux64-2023.3.0.tar.gz"
  script:
    - export COVERITY_URL=https://cnc.chuckaude.com
    - curl -fLsS $COVERITY_URL/api/v2/scans/downloads/$CTC_TOOLKIT | tar -C /tmp -xzf -
    - export COVERITY_CLI_CLOUD_ANALYSIS_ASYNC=false
    - /tmp/bin/coverity scan -o analyze.location=connect -o commit.connect.url=$COVERITY_URL -o commit.connect.stream=$COVERITY_STREAM

.coverity-bridge:
  stage: security
  rules:
    - if: ($CI_COMMIT_BRANCH =~ $SCAN_BRANCHES && $CI_PIPELINE_SOURCE != "merge_request_event")
  variables:
    BRIDGE_COVERITY_CONNECT_URL: "https://cnc.chuckaude.com"
    BRIDGE_COVERITY_CONNECT_USER_NAME: "$COV_USER"
    BRIDGE_COVERITY_CONNECT_USER_PASSWORD: "$COVERITY_PASSWORD"
    BRIDGE_COVERITY_CONNECT_PROJECT_NAME: "$CI_PROJECT_NAME"
    BRIDGE_COVERITY_CONNECT_STREAM_NAME: "$CI_PROJECT_NAME-$CI_COMMIT_BRANCH"
  script:
    - apt-get -qq update && apt-get install -y unzip
    - curl -fLsS -o bridge.zip $BRIDGECLI_LINUX64 && unzip -qo -d /tmp bridge.zip && rm -f bridge.zip
    - /tmp/synopsys-bridge --debug --verbose --stage connect

blackduck-detect:
  stage: security
  rules:
    - if: ($CI_COMMIT_BRANCH =~ $SCAN_BRANCHES && $CI_PIPELINE_SOURCE != "merge_request_event")
  variables:
    DETECT_PROJECT_NAME: "chuckaude-$CI_PROJECT_NAME"
    DETECT_PROJECT_VERSION_NAME: "chuckaude-$CI_COMMIT_BRANCH"
    DETECT_CODE_LOCATION_NAME: "chuckaude-$CI_PROJECT_NAME-$CI_COMMIT_BRANCH"
    DETECT_EXCLUDED_DETECTOR_TYPES: 'GIT'
  script:
    - curl -fLsS -o /tmp/detect8.sh https://detect.synopsys.com/detect8.sh && chmod +x /tmp/detect8.sh
    - /tmp/detect8.sh --detect.policy.check.fail.on.severities=NONE --detect.risk.report.pdf=true

.blackduck-bridge:
  stage: security
  rules:
    - if: ($CI_COMMIT_BRANCH =~ $SCAN_BRANCHES && $CI_PIPELINE_SOURCE != "merge_request_event")
  variables:
    BRIDGE_BLACKDUCK_URL: "$BLACKDUCK_URL"
    BRIDGE_BLACKDUCK_TOKEN: "$BLACKDUCK_API_TOKEN"
    DETECT_PROJECT_NAME: "chuckaude-$CI_PROJECT_NAME"
    DETECT_PROJECT_VERSION_NAME: "chuckaude-$CI_COMMIT_BRANCH"
    DETECT_CODE_LOCATION_NAME: "chuckaude-$CI_PROJECT_NAME-$CI_COMMIT_BRANCH"
    DETECT_EXCLUDED_DETECTOR_TYPES: 'GIT'
    DETECT_POLICY_CHECK_FAIL_ON_SEVERITIES: 'BLOCKER'
  script:
    - apt-get -qq update && apt-get install -y unzip
    - curl -fLsS -o bridge.zip $BRIDGECLI_LINUX64 && unzip -qo -d /tmp bridge.zip && rm -f bridge.zip
    - /tmp/synopsys-bridge --verbose --stage blackduck

coverity-on-polaris:
  stage: security
  rules:
    - if: ($CI_COMMIT_BRANCH =~ $SCAN_BRANCHES && $CI_PIPELINE_SOURCE != "merge_request_event")
  script:
    - apt-get -qq update && apt-get install -y unzip
    - curl -fLOsS $POLARIS_SERVER_URL/api/tools/polaris_cli-linux64.zip
    - unzip -d /tmp -jo polaris_cli-linux64.zip && rm -f polaris_cli-linux64.zip
    - /tmp/polaris --co project.name=chuckaude-$CI_PROJECT_NAME analyze -w

.polaris:
  stage: security
  rules:
    - if: ($CI_COMMIT_BRANCH =~ $SCAN_BRANCHES && $CI_PIPELINE_SOURCE != "merge_request_event")
  variables:
    BRIDGE_POLARIS_APPLICATION_NAME: "chuckaude-$CI_PROJECT_NAME"
    BRIDGE_POLARIS_PROJECT_NAME: "chuckaude-$CI_PROJECT_NAME"
  script:
    - apt-get -qq update && apt-get install -y unzip
    - curl -fLsS -o bridge.zip $BRIDGECLI_LINUX64 && unzip -qo -d /tmp bridge.zip && rm -f bridge.zip
    - /tmp/synopsys-bridge --stage polaris polaris.assessment.types=SAST,SCA
